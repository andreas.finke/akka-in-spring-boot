package org.johnnypark.akkainspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AkkaInSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(AkkaInSpringApplication.class, args);
	}

}
